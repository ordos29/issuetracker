<?php

namespace App\Entity;

use App\Repository\AuditRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IssueRepository")
 * @HasLifecycleCallbacks
 */
class Issue
{
    const STATUS_DELETED = 0;
    const STATUS_CREATED = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_CHECK = 3;
    const STATUS_CLOSED = 4;

    public function getStatusName():?string
    {
        $statusNames = [
            self::STATUS_CREATED => "Создана",
            self::STATUS_IN_PROGRESS => "В работе",
            self::STATUS_CHECK => "На проверке",
            self::STATUS_CLOSED => "Закрыта",
        ];

        return $statusNames[$this->status];
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $short_description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $full_description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $result;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="issues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="performingIssues")
     */
    private $performer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Audit", mappedBy="issue")
     */
    private $audits;

    public function __construct()
    {
        $this->audits = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->short_description;
    }

    public function setShortDescription(?string $short_description): self
    {
        $this->short_description = $short_description;

        return $this;
    }

    public function getFullDescription(): ?string
    {
        return $this->full_description;
    }

    public function setFullDescription(?string $full_description): self
    {
        $this->full_description = $full_description;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function setResult(?string $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->create_date;
    }

    public function setCreateDate(\DateTimeInterface $create_date): self
    {
        $this->create_date = $create_date;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getPerformer(): ?User
    {
        return $this->performer;
    }

    public function setPerformer(?User $performer): self
    {
        $this->performer = $performer;

        return $this;
    }

    /**
     * @return Collection|Audit[]
     */
    public function getAudits(): Collection
    {
        return $this->audits;
    }

    public function addAudit(Audit $audit): self
    {
        if (!$this->audits->contains($audit)) {
            $this->audits[] = $audit;
            $audit->setIssue($this);
        }

        return $this;
    }

    public function removeAudit(Audit $audit): self
    {
        if ($this->audits->contains($audit)) {
            $this->audits->removeElement($audit);
            // set the owning side to null (unless already changed)
            if ($audit->getIssue() === $this) {
                $audit->setIssue(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     * @param LifecycleEventArgs $event
     */
    public function audit(LifecycleEventArgs $event)
    {
        $entityManager = $event->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();
        $changedFields = $unitOfWork->getEntityChangeSet($this);
        if (array_key_exists('status', $changedFields)) {
            $auditRepository = $entityManager->getRepository(Audit::class);
            $auditRepository->addAuditForIssue($this);
        }
    }


    public function fillNewIssue($creator)
    {
        $this->setStatus(self::STATUS_CREATED);
        $this->setCreator($creator);
        $this->setCreateDate(new \DateTime());
    }

    /**
     * @param User $user
     * @return array
     */
    public function getStatusChoices(User $user)
    {
        $choices = [
            'Created' => 1,
            'In progress' => 2,
        ];
        $roleAdmin = in_array('ROLE_ADMIN', $user->getRoles());
        if ($this->getPerformer() && $this->getPerformer()->getId() == $user->getId() || $roleAdmin) {
            $choices['On check'] = 3;
        }
        if ($roleAdmin) {
            $choices['Closed'] = 4;
        };

        return $choices;
    }

    public function checkResult(FormInterface &$form): bool
    {
        $statusCheck = $this->status == Issue::STATUS_CHECK;
        if ($statusCheck && empty($this->result)) {
            $form
                ->get('result')
                ->addError(new FormError('При передаче на проверку должен быть заполнен результат работы'));
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function takeToWork(User $user): bool
    {
        $issueStatusCreated = $this->getStatus() == Issue::STATUS_CREATED;
        $userNotAdmin = !in_array('ROLE_ADMIN', $user->getRoles());
        if ($issueStatusCreated && $userNotAdmin) {
            $this->setPerformer($user);
            $this->setStatus(Issue::STATUS_IN_PROGRESS);

            return true;
        }

        return false;
    }

}
