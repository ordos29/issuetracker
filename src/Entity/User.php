<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $userfio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Issue", mappedBy="creator")
     */
    private $issues;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Issue", mappedBy="performer")
     */
    private $performingIssues;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Audit", mappedBy="user")
     */
    private $audits;

    public function __construct()
    {
        parent::__construct();
        $this->issues = new ArrayCollection();
        $this->performingIssues = new ArrayCollection();
        $this->audits = new ArrayCollection();
    }

    public function getUserfio(): ?string
    {
        return $this->userfio;
    }

    public function setUserfio(?string $userfio): self
    {
        $this->userfio = $userfio;

        return $this;
    }

    /**
     * @return Collection|Issue[]
     */
    public function getIssues(): Collection
    {
        return $this->issues;
    }

    public function addIssue(Issue $issue): self
    {
        if (!$this->issues->contains($issue)) {
            $this->issues[] = $issue;
            $issue->setCreator($this);
        }

        return $this;
    }

    public function removeIssue(Issue $issue): self
    {
        if ($this->issues->contains($issue)) {
            $this->issues->removeElement($issue);
            // set the owning side to null (unless already changed)
            if ($issue->getCreator() === $this) {
                $issue->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Issue[]
     */
    public function getPerformingIssues(): Collection
    {
        return $this->performingIssues;
    }

    public function addPerformingIssue(Issue $performingIssue): self
    {
        if (!$this->performingIssues->contains($performingIssue)) {
            $this->performingIssues[] = $performingIssue;
            $performingIssue->setPerformer($this);
        }

        return $this;
    }

    public function removePerformingIssue(Issue $performingIssue): self
    {
        if ($this->performingIssues->contains($performingIssue)) {
            $this->performingIssues->removeElement($performingIssue);
            // set the owning side to null (unless already changed)
            if ($performingIssue->getPerformer() === $this) {
                $performingIssue->setPerformer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Audit[]
     */
    public function getAudits(): Collection
    {
        return $this->audits;
    }

    public function addAudit(Audit $audit): self
    {
        if (!$this->audits->contains($audit)) {
            $this->audits[] = $audit;
            $audit->setUser($this);
        }

        return $this;
    }

    public function removeAudit(Audit $audit): self
    {
        if ($this->audits->contains($audit)) {
            $this->audits->removeElement($audit);
            // set the owning side to null (unless already changed)
            if ($audit->getUser() === $this) {
                $audit->setUser(null);
            }
        }

        return $this;
    }
}