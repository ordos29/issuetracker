<?php

namespace App\Repository;

use App\Entity\Issue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Kilik\TableBundle\Components\Column;
use Kilik\TableBundle\Components\Filter;
use Kilik\TableBundle\Components\FilterCheckbox;
use Kilik\TableBundle\Components\FilterSelect;
use Kilik\TableBundle\Components\Table;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Issue|null find($id, $lockMode = null, $lockVersion = null)
 * @method Issue|null findOneBy(array $criteria, array $orderBy = null)
 * @method Issue[]    findAll()
 * @method Issue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IssueRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Issue::class);
    }

    /**
     * @param string $ajaxUrl
     * @param int $userId
     * @return Table
     */
    public function getIssueTable(string $ajaxUrl, int $userId): Table
    {
        $queryBuilder = $this
            ->createQueryBuilder('i')
            ->select("i, c, p")
            ->leftJoin('i.creator', 'c')
            ->leftJoin('i.performer', 'p')
            ->andwhere('i.status > 0');

        $table = (new Table())
            ->setId('issues_list')
            ->setPath($ajaxUrl)
            ->setTemplate('issue/_list.html.twig')
            ->setTemplateParams(['userId' => $userId])
            ->setQueryBuilder($queryBuilder, 'i')
            ->addColumn(
                (new Column())->setLabel('Название')
                    ->setSort(['i.name' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('i.name')
                        ->setName('i_name')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('Исполнитель')
                    ->setSort(['p.userfio' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('p.userfio')
                        ->setName('p_userfio')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('Создатель')
                    ->setSort(['c.userfio' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('c.userfio')
                        ->setName('c_userfio')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('Дата создания')
                    ->setSort(['i.create_date' => 'asc'])
                    ->setDisplayFormat(Column::FORMAT_DATE)
                    ->setDisplayFormatParams('Y-m-d H:i:s')
                    ->setFilter((new Filter())
                        ->setField('i.create_date')
                        ->setName('i_create_date')
                        ->setDataFormat(Filter::FORMAT_DATE)
                    )
            )
            ->addColumn(
                (new Column())->setLabel('Текщий статус')
                    ->setSort(['i.status' => 'asc'])
                    ->setDisplayCallback(function ($value) {
                        switch ($value) {
                            case Issue::STATUS_CREATED:
                                return "Создана";
                            case Issue::STATUS_IN_PROGRESS:
                                return "В работе";
                            case Issue::STATUS_CHECK:
                                return "На проверке";
                            case Issue::STATUS_CLOSED:
                                return "Закрыта";
                            default:
                                return $value;
                        }
                    })
                    ->setFilter((new FilterSelect())
                        ->setField('i.status')
                        ->setName('i_status')
                        ->setChoices(
                            [
                                'Created' => Issue::STATUS_CREATED,
                                'In progress' => Issue::STATUS_IN_PROGRESS,
                                'On check' => Issue::STATUS_CHECK,
                                'Closed' => Issue::STATUS_CLOSED,
                            ])
                        ->setPlaceholder('All')
                    )
            )->addFilter(
                (new FilterCheckbox())
                    ->setField('p.id')
                    ->setName('i_performer')
                    ->setQueryPartBuilder(function (Filter $filter, Table $table, \Doctrine\ORM\QueryBuilder $queryBuilder, $value) use ($userId) {
                        $queryBuilder->andWhere('p.id = ' . $userId)
                            ->andWhere('i.status = ' . Issue::STATUS_IN_PROGRESS . ' or i.status = ' . Issue::STATUS_CHECK);
                    })
            );

        return $table;
    }

}
