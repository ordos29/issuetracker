<?php

namespace App\Repository;

use App\Entity\Audit;
use App\Entity\Issue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Kilik\TableBundle\Components\Column;
use Kilik\TableBundle\Components\Filter;
use Kilik\TableBundle\Components\FilterSelect;
use Kilik\TableBundle\Components\Table;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Audit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Audit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Audit[]    findAll()
 * @method Audit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuditRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Audit::class);
    }

    public function addAuditForIssue(Issue $issue)
    {
        $audit = new Audit();
        $audit->setUser($issue->getPerformer() ?: $issue->getCreator());
        $audit->setDate(new \DateTime('now'));
        $audit->setIssueStatus($issue->getStatus());
        $audit->setIssue($issue);
        $this->getEntityManager()->persist($audit);
        $this->getEntityManager()->flush();
    }

    /**
     * @param string $ajaxUrl
     * @return Table
     */
    public function getAuditTable(string $ajaxUrl): Table
    {
        $queryBuilder = $this
            ->createQueryBuilder('a')
            ->select("a, u, i")
            ->leftJoin('a.user', 'u')
            ->leftJoin('a.issue', 'i')
            ->andWhere('i.status > 0');

        $table = (new Table())
            ->setId('audit_list')
            ->setPath($ajaxUrl)
            ->setQueryBuilder($queryBuilder, 'a')
            ->addColumn(
                (new Column())->setLabel('Пользователь')
                    ->setSort(['u.userfio' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('u.userfio')
                        ->setName('u_userfio')
                    )
            )
            ->addColumn(
                (new Column())->setLabel('Дата')
                    ->setSort(['a.date' => 'asc'])
                    ->setDisplayFormat(Column::FORMAT_DATE)
                    ->setDisplayFormatParams('Y-m-d H:i:s')
                    ->setFilter((new Filter())
                        ->setField('a.date')
                        ->setName('a_date')
                        ->setDataFormat(Filter::FORMAT_DATE)
                    )
            )
            ->addColumn(
                (new Column())->setLabel('Установленный статус')
                    ->setSort(['a.issueStatus' => 'asc'])
                    ->setDisplayCallback(function ($value) {
                        switch ($value) {
                            case Issue::STATUS_CREATED:
                                return "Создана";
                            case Issue::STATUS_IN_PROGRESS:
                                return "В работе";
                            case Issue::STATUS_CHECK:
                                return "На проверке";
                            case Issue::STATUS_CLOSED:
                                return "Закрыта";
                            default:
                                return $value;
                        }
                    })
                    ->setFilter((new FilterSelect())
                        ->setField('a.issueStatus')
                        ->setName('a_issueStatus')
                        ->setChoices(
                            [
                                'Создана' => Issue::STATUS_CREATED,
                                'В работе' => Issue::STATUS_IN_PROGRESS,
                                'На проверке' => Issue::STATUS_CHECK,
                                'Закрыта' => Issue::STATUS_CLOSED,
                            ])
                        ->setPlaceholder('-- Все --')
                        ->disableTranslation()
                    )
            )
            ->addColumn(
                (new Column())->setLabel('Номер заявки')
                    ->setSort(['i.id' => 'asc'])
                    ->setFilter((new Filter())
                        ->setField('i.id')
                        ->setName('i_id')
                    )
            );

        return $table;
    }
}
