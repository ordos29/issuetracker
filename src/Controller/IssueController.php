<?php

namespace App\Controller;

use App\Entity\Issue;
use App\Form\IssueEditType;
use App\Form\IssueNewType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IssueController extends Controller
{
    /**
     * @Route("/", name="homepage", methods="GET")
     */
    public function index(): Response
    {
        $issueTable = $this->getDoctrine()
            ->getRepository('App:Issue')
            ->getIssueTable($this->generateUrl('issues_list_ajax'), $this->getUser()->getId());

        return $this->render('issue/list.html.twig', ['table' => $this->get('kilik_table')->createFormView($issueTable),]);
    }

    /**
     * @Route("/issue/create", name="create-issue", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $issue = new Issue();
        $form = $this->createForm(IssueNewType::class, $issue);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $issue->fillNewIssue($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($issue);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('issue/new.html.twig', [
            'issue' => $issue,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/issue/{id}", name="issue_show", methods="GET")
     * @param Issue $issue
     * @return Response
     */
    public function show(Issue $issue): Response
    {
        if ($issue->getStatus() == 0) {
            throw $this->createNotFoundException('Заявка не найдена');
        }

        return $this->render('issue/show.html.twig', [
            'issue' => $issue,
        ]);
    }

    /**
     * @Route("/audit", name="audit", methods="GET")
     */
    public function audit(): Response
    {
        $auditTable = $this->getDoctrine()
            ->getRepository('App:Audit')
            ->getAuditTable($this->generateUrl('audit_list_ajax'));

        return $this->render('audit/list.html.twig', [
            'table' => $this->get('kilik_table')->createFormView($auditTable),
        ]);
    }

    /**
     * @Route("/issue/{id}/edit", name="issue_edit", methods="GET|POST")
     * @param Request $request
     * @param Issue $issue
     * @return Response
     */
    public function edit(Request $request, Issue $issue): Response
    {
        if ($issue->getStatus() == 0) {
            throw $this->createNotFoundException('Заявка не найдена');
        }

        $form = $this->createForm(IssueEditType::class, $issue, ['user' => $this->getUser(),]);
        $issueResult = $issue->getResult();
        $form->handleRequest($request);
        $resultChanged = $issueResult != $issue->getResult();
        if ($form->isSubmitted() && $form->isValid()) {
            $currentUserIsPerformer = $issue->getPerformer() && $issue->getPerformer()->getId() == $this->getUser()->getId();
            $roleAdmin = in_array('ROLE_ADMIN', $this->getUser()->getRoles());
            if (!$currentUserIsPerformer && !$roleAdmin) {
                throw $this->createAccessDeniedException('Вы не являетесь исполнителем данной заявки');
            }
            if ($issue->getStatus() == Issue::STATUS_CLOSED && !$roleAdmin){
                throw $this->createAccessDeniedException('Закрыть задачу может только супервайзер');
            }
            if ($resultChanged && $roleAdmin && !in_array($issue->getStatus(), [Issue::STATUS_CHECK, Issue::STATUS_CLOSED])) {
                throw $this->createAccessDeniedException('Нельзя изменит результат для статусов Создана и В работе');
            }
            if ($issue->checkResult($form)) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('issue_edit', ['id' => $issue->getId()]);
            }

        }

        return $this->render('issue/edit.html.twig', [
            'issue' => $issue,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/issue/{id}/takeToWork", name="take_to_work", methods="POST")
     * @param Request $request
     * @param Issue $issue
     * @return RedirectResponse
     */
    public function takeToWork(Request $request, Issue $issue): RedirectResponse
    {
        if ($issue->takeToWork($this->getUser())) {
            $this->getDoctrine()->getManager()->flush();
        } else {
            throw $this->createAccessDeniedException("Не возможно взять заявку в работу");
        }

        return $this->redirectToRoute('issue_edit', ['id' => $issue->getId()]);
    }

    /**
     * @Route("/issue/{id}", name="issue_delete", methods="DELETE")
     */
    public function delete(Request $request, Issue $issue): Response
    {
        if ($this->isCsrfTokenValid('delete' . $issue->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $issue->setStatus(Issue::STATUS_DELETED);
            $em->persist($issue);
            $em->flush();
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/list_ajax", name="issues_list_ajax")
     */
    public function _listAction(Request $request)
    {
        $issueTable = $this->getDoctrine()
            ->getRepository('App:Issue')
            ->getIssueTable($this->generateUrl('issues_list_ajax'), $this->getUser()->getId());

        return $this->get('kilik_table')->handleRequest($issueTable, $request);
    }

    /**
     * @Route("/audit_list_ajax", name="audit_list_ajax")
     */
    public function _auditListAction(Request $request)
    {
        $auditTable = $this->getDoctrine()
            ->getRepository('App:Audit')
            ->getAuditTable($this->generateUrl('audit_list_ajax'));

        return $this->get('kilik_table')->handleRequest($auditTable, $request);
    }
}
