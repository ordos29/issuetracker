<?php

namespace App\Form;

use App\Entity\Issue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IssueEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $issue = $options['data'];

        $choices = $issue->getStatusChoices($user);
        $roleAdmin = !is_null($user) && in_array('ROLE_ADMIN', $user->getRoles());
        if (!$roleAdmin || in_array($issue->getStatus(), [Issue::STATUS_CHECK, Issue::STATUS_CLOSED])) {
            $builder->add('status', ChoiceType::class, ['choices' => $choices,]);
        }

        $builder
            ->add('name', TextType::class)
            ->add('full_description', TextareaType::class);

        $issueStatusCheck = $issue->getStatus() == Issue::STATUS_CHECK;
        $issueStatusClosed = $issue->getStatus() == Issue::STATUS_CLOSED;

        if (($roleAdmin && ($issueStatusCheck || $issueStatusClosed)) || !$roleAdmin) {
            $builder->add('result', TextareaType::class, ['required' => false,]);
        }
        $builder->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Issue::class,
            'user' => null,
        ]);
    }
}
